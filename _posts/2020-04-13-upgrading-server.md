---
layout: blog_post
title: "Migrating to a new machine"
category: developer
author: Lekro
short: "CubeKrowd is now running on a new machine!"
tags:
  - server
---

CubeKrowd is now running on a new machine! This is basically a CPU upgrade
with the goal of better TPS everywhere on CK, but we have now moved from
France to Germany as well. In this blog post, we wish to share the hardware
specifications of the new server, mainly for the curious!

<!--more-->
## New server hardware
The new machine is hosted in Falkenstein, Germany by Hetzner,
and has the following specifications.
- CPU: AMD Ryzen 5 3600 (6 cores, up from the 4-core i7-6700k)
- RAM: 64 GB DDR4 (the same)
- Network bandwidth: 1 Gbit/s (up from 250 Mbit/s)

## The migration process
Overall, the migration went smoothly, despite a number of possible things
that could have gone wrong! Just for starters, when we placed the order
on the new server, Hetzner had the following message on their website,
so we weren't sure how long it would take for them to actually provide
access to the new machine.


> **Serious delivery wait time - no guaranteed delivery date**
> 
> Important note: Due to unexpectedly high demand, limited supplies, and
> logistical problems beyond our control (Coronavirus), we will place your
> order on our waitlist and do our best to fill it as quickly as possible.


But as it turned out, there was nothing to worry about because Hetzner
delivered the server anyway! Now that we had access to the new machine,
the next step was to transfer all the data from the old machine to the
new one. To save on downtime, data migration was split into three
basic steps.

1. Use `rsync` to copy live copies of everything (databases, MC servers,
etc.) onto the new server.
2. Test everything on the new server. (everything worked!)
3. Shut down all servers on the old and new machines, run `rsync` again
to copy latest versions of all data. Update DNS records. Start up servers
on new machine.

After step 3, the Minecraft servers and essential services were all up
and working, with much better TPS than before!
