---
layout: blog_post
title: "The website gets a facelift"
category: website
author: Foorack
short: "We are currently restyling the website. Not all functions are ready but we hope you will love it! Happy new year everyone! <i class=\"em em-fireworks\"></i>"
tags:
  - website
---

First of all, I want to congratulate, **happy new year everyone!!** 2016 was an
amazing year but let’s try and make 2017 even better, but with a such lovely
community I don't know if that's even possible. In celebration for the start
of the new year, we've decided to update and modernise our web services
starting with the homepage.
<!--more-->

As time passes by quicker than ever before our current websites are outdated
and in desperate need for maintenance and redesigning. The previous homepage
is nearly two years old and the forum is still using the same old theme
since its creation. We want to deliver only the absolute best experience and
to do this we will in the coming months work towards updating and improving
the rest of our websites.

**The new website is designed with mobile-compatibility in mind.** The entire
website is responsive which means it works and should look good on all devices
from phones to tables and TV's. We have also recently deployed HTTPS on all
domains which is techy talk for saying that all of your online communications
with `cubekrowd.net` is secured and can no longer be read by evil hackers or
your ISP. We will do another blog in the future going into the specifics of
this for those who are interested.

Until then I would like to thank you for reading this blog, thank you for coming
to the server every day and thank you for making it the best place in the world.
I strongly look forward to see what 2017 (12017) will bring. New maps, new staff,
maybe a new game mode or two? Anyways, that's all for now.

*Stay awesome everyone.* <i class="em em-sunglasses"></i>
