---
layout: blog_post
title: CubeKrowd launches Official Discord Server!
category: community
author: Foorack
short: "Our official discord was finally released. This means better voice quality and lots of fancy features. <a href='https://discord.cubekrowd.net'>Join here!</a>"
tags:
  - discord
---

For a rather long time now TeamSpeak has been our main way of communicating by
voice. It is the place where many of us gather to hang out and talk with one
another. TeamSpeak has served us well but since we started using it other
programs have come along bringing better voice quality and shiny new features.
One of them is Discord which is what I will talk about today.
<!--more-->

[Discord](https://en.wikipedia.org/wiki/Discord_(software)) is an all-in-one
voice and text chat for gamers that's free and secure. It also works on both
desktop, mobile and web.

## Usage
There are three different ways you can get Discord. The desktop and web versions
are mostly the same with the same interface layout. If you have used Discord in
the past, then just skip this section.

#### Web
The web interface is the easiest as it does not require you to download nor
install anything. Start by clicking this link and go to
[discord.cubekrowd.net](https://discord.cubekrowd.net){:target="_blank"}.
This will give you an instant invite to the server. Discord will ask what they
should call you, this will be your username, you can change this later if you
want. It helps if you use the same username as in game and on Discord, however,
this is not required.

Once you join we recommend you to follow Discord's beginner tutorial which
should pop-up right when you join. We also recommend you to experiment,
play around with the interface. If you have any questions, then try Google the
problem and you will find someone else has most likely already asked it.

#### Desktop
Discord is especially meant for gamers, which means there are a lot of goodies
ready to enhance your experience, for example game-integration and push-to-talk
without having to focus on the browser. Discord can also automatically detect
what game you are playing and show it under your name in the user list. You can
disable automatic game-detection by clicking on the cog-wheel next to your
username, go to the *"Games"* tab and uncheck the checkbox at the top.
To download Discord just [click this link and
select your platform](https://discordapp.com/download){:target="_blank"}
*(scroll down for platform selection)*. It
is really easy and works both on Windows and Mac. A Linux version is under
development.


#### Mobile
The Discord mobile app is completely free and currently works on both Android
and iOS. You can get it either by clicking the download link above or by
following either of these direct links which will lead you directly to the
download centre for respective platform.

  * Download for Android: [https://play.google.com/store/apps/details?id=com.discord](https://play.google.com/store/apps/details?id=com.discord)
  * Download for iOS: [https://itunes.apple.com/us/app/discord-chat-for-games/id985746746](https://itunes.apple.com/us/app/discord-chat-for-games/id985746746)

The interface for mobile devices is obviously a little different but most of it
is the same. Not all features exist yet on mobile but they are coming. One known
but is that users will sometimes be displayed with just a white name instead of
their ranks colour.

## Linkup
At CubeKrowd we use a custom linkup system to link your Minecraft and Discord
accounts. This makes us able to automatically set your rank, and update it
when you get promoted. Linking is required for you to be able to chat and use
join voice channels. You can find information on how to linkup in the
`#linkup` channel but for simplicity we will explain it here as well.

  1. Find the `#linkup` channel. Text and voice channels are not connected like
they are in TeamSpeak. You can be in one voice channel and at the same time
switch between different text channels. The `#linkup` channel is only visible if
you are not already linked. This channel is the one located highest up in the
channels list. Once you have found the channel we recommend you to read the text
and follow the instructions.
  2. Hop on to CubeKrowd and once you join, do `/discord` and you will get a 4
number code. Remember this code for a min and do not share it with anyone else!
  3. Go back to the `#linkup` channel and type the following command, replace
with the code you got in the previous step: `!linkup 1234` If you entered the
correct code you will be given your rank and the #linkup channel will disappear.

That's it! Congratulations, if you have followed the instructions you have now
successfully linked up. This is a one-time process and you never have to do it
again. Your rank will automatically be updated if you get promoted or donate.

## Emojis
Oh, by the way. **WE GOT EMOJIS!!!** <i class="em em-medal"></i> <i class="em em-video_game"></i> <i class="em em-shield"></i>
Yup, and not only that. Discord even gives us up to 50 customisable emoji's.
We are currently in the process of selecting which ones we should
have. So far we have the admins (and letsgo's) heads and the CK logo but this
may change. If you have suggestions on how we can improve please click on the
"Suggest a Feature" under Resources below. Due to the number of responses we
cannot respond to everyone but we do read all the replies.

**The Discord server is still in Beta.** At the time of writing the `#rules`
channel is empty but remember that *Global rules always applies*. In regard to
TeamSpeak nothing is yet decided what will happen to it. Until further notice it
will be up and running 24/7 as it always has. It will be interesting to see all
the cool stuff we can do with Discord (we have our own bot!) and I look forward
seeing you on there.
