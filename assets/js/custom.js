---
---
// Double triple-dash MUST BE HERE for jekyll, and they MUST NOT BE INDENTED.

/*	################################################################

    ck-website
    Copyright (C) 2020  CubeKrowd Network

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

################################################################# */

const bansSearchData = {}
{% for record in site.data.bans_users %} bansSearchData["username+{{ record.username }}"] = "{{ record.uuid }}"; {% endfor %}

(function ($) {

    jQuery(document).ready(function () {
        $('[data-toggle="popover"]').popover();
        $('.popover-dismiss').popover({
            trigger: 'focus'
        });
    });

})(jQuery);

function dropdownNav(e) {
    $($($('#header-nav-inner').children().get(0)).children().get(0)).toggleClass("fa-chevron-up");
    $($($('#header-nav-inner').children().get(0)).children().get(0)).toggleClass("fa-chevron-down");

    $($('#header-nav-inner').children().get(2)).toggleClass("d-none d-md-block");
    $($('#header-nav-inner').children().get(3)).toggleClass("d-none d-md-block");
    $($('#header-nav-inner').children().get(4)).toggleClass("d-none d-md-block");
    $($('#header-nav-inner').children().get(5)).toggleClass("d-none d-md-block");
    $($('#header-nav-inner').children().get(6)).toggleClass("d-none d-md-block");
}

function findBansProfile(searchQuery, callbackSuccess, callbackNotFound) {
    searchQuery = searchQuery.toLowerCase();
    // Try optimistically cutting off
    // Foorakc NO
    // Foorak NO
    // Foora OK
    var suggestions = [];
    A: for (var i = searchQuery.length; i >= 2; i--) {
        for (var username in bansSearchData) {
            if (bansSearchData.hasOwnProperty(username)) {
                if (username.toLowerCase().startsWith("username+" + searchQuery.substring(0, i))) {
                    // Perfect match?
                    if (i == searchQuery.length && username.toLowerCase() == "username+" + searchQuery) {
                        callbackSuccess(bansSearchData[username]);
                        return;
                    }
                    if (!suggestions.includes(username)) {
                        suggestions.push(username);
                    }
                    if (suggestions.length == 5) {
                        break A;
                    }
                }
            }
        }

    }
    callbackNotFound(suggestions);
}

function onSubmitBansSearch(e) {
    var searchQuery = document.bansSearchForm.bansSearchUsername.value.replace(/\W/g, '');

    findBansProfile(searchQuery, (uuid) => {
        window.location.href = "/bans/" + uuid;
    }, (suggestions) => {
        $('#bansSearchError').removeClass("d-none");
        if (suggestions.length == 0) {
            $('#bansSearchError').html("Player '" + searchQuery + "' not found.");
        } else {
            var suggestionLinks = suggestions.map(username => "<a class='text-white font-weight-bold' href='/bans/" + bansSearchData[username] + "'><u>" + username.split("+")[1] + "</u></a>").join(", ");
            $('#bansSearchError').html("<i class='em em-mag_right mr-2'></i>Player '" + searchQuery + "' not found. Did you mean " + suggestionLinks + "?");
        }
    });
    return false;
}